package grpcsslauth

import (
	"crypto/tls"
	"fmt"
	"log"
	"net"
	"time"

	//authserverpb "gitlab.com/k.kulvik/grpcssl/authserver"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/keepalive"
)

var (
	insecure      bool    = false
	cacert        string  = ""
	cert          string  = ""
	key           string  = ""
	maxTime       float64 = 20 //default is 10s
	keepaliveTime float64 = 10
	plaintext     bool    = false
	addrGKE       string  = "auth.zenniz.com:443"

)

type ClientConnector struct {
	CliCon *grpc.ClientConn
	CTX    context.Context
}

func InitClient() ClientConnector {

	var CC ClientConnector

	ctx := context.Background()
	if maxTime > 0 {
		timeout := time.Duration(maxTime * float64(time.Second))
		ctx, _ = context.WithTimeout(ctx, timeout)
	}

	//dial := func() *grpc.ClientConn {

	var opts []grpc.DialOption
	if keepaliveTime > 0 {
		timeout := time.Duration(keepaliveTime * float64(time.Second))
		opts = append(opts, grpc.WithKeepaliveParams(keepalive.ClientParameters{
			Time:    timeout,
			Timeout: timeout,
		}))
	}

	var creds credentials.TransportCredentials
	if !plaintext {

		var tlsConf tls.Config

		creds = credentials.NewTLS(&tlsConf)

	}
	network := "tcp"

	//cc := BlockingDial(ctx, network, AddrGKE, creds, opts...)
	//return cc
	//}

	if creds != nil {
		creds = &errSignalingCreds{
			TransportCredentials: creds,
		}
	}
	dialer := func(ctx context.Context, address string) (net.Conn, error) {

		conn, err := (&net.Dialer{}).DialContext(ctx, network, address)
		if err != nil {
			log.Printf(err.Error())
		}
		return conn, err
	}

	opts = append(opts,
		grpc.WithBlock(),
		grpc.FailOnNonTempDialError(true),
		grpc.WithContextDialer(dialer),
	)

	opts = append(opts, grpc.WithTransportCredentials(creds))

	c1 := make(chan *grpc.ClientConn)

	go func() {

		conn, err := grpc.DialContext(ctx, addrGKE, opts...)

		if err != nil {
			log.Printf(err.Error())
		}
		c1 <- conn
	}()

	conns := <-c1

	if conns != nil {
		CC.CliCon = conns
		CC.CTX = ctx
	}

	return CC
}

type errSignalingCreds struct {
	credentials.TransportCredentials
}

func (c *errSignalingCreds) ClientHandshake(ctx context.Context, addr string, rawConn net.Conn) (net.Conn, credentials.AuthInfo, error) {
	conn, auth, err := c.TransportCredentials.ClientHandshake(ctx, addr, rawConn)

	if err != nil {
		fmt.Println(err.Error())
	}
	return conn, auth, err
}

func BlockingDial(ctx context.Context, network, address string, creds credentials.TransportCredentials, opts ...grpc.DialOption) (conns *grpc.ClientConn) {

	//result := make(chan interface{}, 1)

	/*
		writeResult := func(res interface{}) {
			select {
			case result <- res:
			default:
			}
		}*/

	if creds != nil {
		creds = &errSignalingCreds{
			TransportCredentials: creds,
		}
	}
	dialer := func(ctx context.Context, address string) (net.Conn, error) {

		conn, err := (&net.Dialer{}).DialContext(ctx, network, address)
		if err != nil {
			log.Printf(err.Error())
		}
		return conn, err
	}

	opts = append(opts,
		grpc.WithBlock(),
		grpc.FailOnNonTempDialError(true),
		grpc.WithContextDialer(dialer),
	)

	opts = append(opts, grpc.WithTransportCredentials(creds))

	c1 := make(chan *grpc.ClientConn)
	go func() {

		conn, err := grpc.DialContext(ctx, address, opts...)

		if err != nil {
			log.Printf(err.Error())
		}

		c1 <- conn
	}()

	conns = <-c1
	/*
		select {
		case res := <-result:
			if conn, ok := res.(*grpc.ClientConn); ok {
				return conn, nil
			}
			return nil, res.(error)
		case <-ctx.Done():
			return nil, ctx.Err()
		}*/
	return
}

/*
func InvokeRpc(ctx context.Context, cc *grpc.ClientConn) error {

	c := authserverpb.NewLocalClient(cc)
	r, err := c.GetToken(ctx, &authserverpb.AuthServerReq{CreatedTime: ptypes.TimestampNow(), DeviceId: "devicetest", Pin: "123456", Email: "testman@testdevice.com", MacAddress: "testmac"})

	if err != nil {
		log.Printf(err.Error())
	}

	log.Printf("idToken %s:", r.GetIdToken())
	log.Printf("idToken %s:", r.GetRefreshToken())

	return err

}
*/
