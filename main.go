package main

import (
	"log"

	"github.com/golang/protobuf/ptypes"

	authserverpb "gitlab.com/k.kulvik/grpcssl/authserver"
	"gitlab.com/k.kulvik/grpcssl/grpcsslauth"
)

/*
var (
	insecure      bool    = false
	cacert        string  = ""
	cert          string  = ""
	key           string  = ""
	maxTime       float64 = 20 //default is 10s
	keepaliveTime float64 = 10
	plaintext     bool    = false
	addrGKE       string  = "courtius.net:443"
	authority     string  = ""
)
*/

func main() {

	CC := grpcsslauth.InitClient()

	/*
		ctx := context.Background()
		if maxTime > 0 {
			timeout := time.Duration(maxTime * float64(time.Second))
			ctx, _ = context.WithTimeout(ctx, timeout)
		}

		dial := func() *grpc.ClientConn {

			var opts []grpc.DialOption
			if keepaliveTime > 0 {
				timeout := time.Duration(keepaliveTime * float64(time.Second))
				opts = append(opts, grpc.WithKeepaliveParams(keepalive.ClientParameters{
					Time:    timeout,
					Timeout: timeout,
				}))
			}

			var creds credentials.TransportCredentials
			if !plaintext {
				var err error

				creds, err = ClientTransportCredentials(insecure, cacert, cert, key)
				if err != nil {
					log.Println(err.Error())
					//fail(err, "Failed to configure transport credentials")
				}
				// can use either -servername or -authority; but not both

				overrideName := authority

				if overrideName != "" {
					if err := creds.OverrideServerName(overrideName); err != nil {
						log.Println(err.Error())
						//fail(err, "Failed to override server name as %q", overrideName)
					}
				}
			} else if authority != "" {
				opts = append(opts, grpc.WithAuthority(authority))
			}
			network := "tcp"

			cc, err := BlockingDial(ctx, network, addrGKE, creds, opts...)
			if err != nil {
				log.Println(err.Error())

				//fail(err, "Failed to dial target host %q", addrGKE)
			}
			return cc
		}*/

	/*
		printFormattedStatus := func(w io.Writer, stat *status.Status, formatter grpcurl.Formatter) {
			formattedStatus, err := formatter(stat.Proto())
			if err != nil {
				fmt.Fprintf(w, "ERROR: %v", err.Error())
			}
			fmt.Fprint(w, formattedStatus)
		}*/

	/*
		if *expandHeaders {
			var err error
			addlHeaders, err = grpcurl.ExpandHeaders(addlHeaders)
			if err != nil {
				fail(err, "Failed to expand additional headers")
			}
			rpcHeaders, err = grpcurl.ExpandHeaders(rpcHeaders)
			if err != nil {
				fail(err, "Failed to expand rpc headers")
			}
			reflHeaders, err = grpcurl.ExpandHeaders(reflHeaders)
			if err != nil {
				fail(err, "Failed to expand reflection headers")
			}
		}
	*/

	//var cc *grpc.ClientConn
	//var descSource grpcurl.DescriptorSource
	//var refClient *grpcreflect.Client
	//var fileSource grpcurl.DescriptorSource
	//if len(protoset) > 0 {

	//} else if len(protoFiles) > 0 {
	//	var err error
	//	fileSource, err = grpcurl.DescriptorSourceFromProtoFiles(importPaths, protoFiles...)
	//	if err != nil {
	//		fail(err, "Failed to process proto source files.")
	//	}

	//}

	/*
		descSource = fileSource

		reset := func() {
			if refClient != nil {
				refClient.Reset()
				refClient = nil
			}
			if cc != nil {
				cc.Close()
				cc = nil
			}
		}
		defer reset()
		exit = func(code int) {
			// since defers aren't run by os.Exit...
			reset()
			os.Exit(code)
		}
	*/

	//if cc == nil {
	//	cc = dial()

	//}
	/*

		err := InvokeRpc(ctx, cc)
		if err != nil {
			log.Println(err.Error())
		}
	*/

	c := authserverpb.NewLocalClient(CC.CliCon)

	r, err := c.GetToken(CC.CTX, &authserverpb.AuthServerReq{CreatedTime: ptypes.TimestampNow(), DeviceId: "_____", Pin: "____", Email: "testman@testdevice.com", MacAddress: "testmac"})

	if err != nil {
		log.Printf(err.Error())
	}

	log.Printf("idToken %s:", r.GetIdToken())
	log.Printf("idToken %s:", r.GetRefreshToken())

}
