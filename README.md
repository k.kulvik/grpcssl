# gRPCSSL package

This is a gRPCSSL package that handles all the handshakes to the server in GKE

## Test proto

Compile proto file: 
```
protoc --go_out=paths=source_relative:. authserverpb/authserver.proto
```

## Import and use

Add dependency to the project

```
import (
    	"gitlab.com/k.kulvik/grpcssl/grpcsslauth"

)
```

Then initialize to get context and connection client

```
    CC := grpcsslauth.InitClient()
```

The use with any pb as
```

c := yourpb.NewServiceClient(CC.CliCon)

	r, err := c.DoSomething(CC.CTX, &yourpb.YourReq{CreatedTime: ptypes.TimestampNow(), MyName: "My Name"})

	if err != nil {
		log.Printf(err.Error())
	}

    log.Printf("value1 %s:", r.GetValue1())
	log.Printf("value2 %s:", r.GetValue2())

```
